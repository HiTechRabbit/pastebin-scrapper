import datetime
import logging
import os
import requests
import re
from random import choice as random_choice
from bs4 import BeautifulSoup
from functools import wraps

import helpers
import settings


class PastebinParser:
    """
    Analyse les derniers fichiers publiques de Pastebin.
    """

    _URL_RAW = 'https://pastebin.com/raw%s'
    _URL = 'https://pastebin.com/archive'
    _MINIMUM_MATCHES = 3

    USER_AGENTS = [
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.6.01001)",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.7.01001)",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.5.01003)",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
                "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0",
                "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/5.0)"
    ]

    _MAIL_PATTERN = r"\"?([-a-zA-Z0-9.`?{}]+@\w+\.\w+)\"?"

    def _reset_header(method):
        """
        Change de manière aléatoire l'user-agent parmi une liste dans self
        """
        @wraps(method)
        def method_wrapper(self, *args, **kwargs):
            self._session.headers = {"User-Agent": random_choice(self.USER_AGENTS)}
            return method(self, *args, **kwargs)

        return method_wrapper

    def __init__(self, required_flags: str, flags: str, exclusion_flags: str):
        """
        Constructeur de parser Pastebin. Télécharge les pastes contenant des flags particuliers.
        :param flags: Mots clefs à chercher
        :param exclusion_flags: Mots clefs ne devant pas être présents
        """
        self._flags = flags
        self._exclusion_flags = exclusion_flags
        self._required_flags = required_flags

        self._session = requests.session()

        # Prépare un répertoire pour les téléchargements
        current_directory = os.path.dirname(os.path.realpath(__file__))
        self._main_directory = "{}/downloads/{}".format(current_directory, datetime.date.today())
        os.makedirs(self._main_directory, exist_ok=True)

        self._logger = logging.Logger(name='parser logs', level=logging.INFO)
        self._mail_logger = logging.Logger(name='mail logs', level=logging.INFO)

        file_handler = logging.FileHandler(self._main_directory + '/bot.log')
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))

        mail_handler = logging.FileHandler(self._main_directory + '/mails.csv')
        mail_handler.setFormatter(logging.Formatter('%(message)s'))

        self._logger.addHandler(file_handler)
        self._mail_logger.addHandler(mail_handler)

    def _download_parse(self, link: str) -> str:
        file_path = self._main_directory + "/" + link.split('/')[-1]

        with open(file_path, "wb") as file:
            response = self._session.get(link)
            file.write(response.content)
        return file_path

    def _extract_mails(self, content: str):
        """
        Extrait les adresses e-mail contenus dans une chaîne
        :param content: Contenu à analyser
        :return:
        """

        mails_extracted = re.findall(self._MAIL_PATTERN, content, re.IGNORECASE)
        for mail in mails_extracted:
            if helpers.is_email_address_valid(mail):
                self._mail_logger.info(mail)

    @_reset_header
    def _save_parse(self, link: str, title: str):
        link = self._URL_RAW % link
        try:
            request_result = self._session.get(link)
            request_result.raise_for_status()
        except requests.RequestException as exception:
            self._logger.error('The HTTP client encountered an error : %s' % exception)

        # Extrait uniquement les données essentielles
        content = str(BeautifulSoup(request_result.content, 'html.parser').extract())

        content_matches = set(re.findall(self._exclusion_flags, content, re.IGNORECASE))
        title_matches = set(re.findall(self._exclusion_flags, title, re.IGNORECASE))
        if content_matches or title_matches:
            self._logger.info('Link {} matches the forbidden flags {}.'.format(link, str(content_matches)))
            return

        content_matches = set(re.findall(self._flags, content, re.IGNORECASE))
        title_matches = set(re.findall(self._flags, title, re.IGNORECASE))
        required_matches = set(re.findall(self._required_flags, title, re.IGNORECASE))
        
        # nombre d'occurences uniques 
        if (len(content_matches) >= self._MINIMUM_MATCHES and
        required_matches) or title_matches:
            try:
                file_path = self._download_parse(link)
                self._extract_mails(content)
                self._logger.warning('Link {} matches the flags {}. Saved in {}'.format(link, str(content_matches), file_path))
            except requests.RequestException as exception:
                self._logger.error('The HTTP client encountered an error : %s' % exception)
            except IOError as exception:
                self._logger.error('Could not write the download content on the disk : %s' % exception)

    @_reset_header
    def retrieve_parses(self):
        try:
            request_result = self._session.get(self._URL)
            request_result.raise_for_status()

            html_parser = BeautifulSoup(request_result.content, 'html.parser')

            main_table = html_parser.find('table', {'class', 'maintable'})
            for row in main_table.find_all('td'):
                try:
                    link = row.find('a').attrs['href']
                    title = row.find('a').text
                    if 'archive' not in link:
                        self._save_parse(link, title)

                except AttributeError:
                    pass

        except Exception as exception:
            self._logger.fatal(exception)
            raise exception


def main():
    # Récupère les derniers pastes
    parser = PastebinParser(settings.REQUIRED_FLAGS, settings.FLAGS, settings.EXCEPT_FLAGS)
    parser.retrieve_parses()

    # Compresse dans une archive ZIP les fichiers téléchargés hier
    current_directory = os.path.dirname(os.path.realpath(__file__))
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    yesterday_path = "{}/downloads/{}".format(current_directory, yesterday)
    if os.path.isdir(yesterday_path):
        zip_path = "{}/{}.zip".format(yesterday_path, yesterday)
        if not os.path.exists(zip_path):
            helpers.compress(yesterday_path, zip_path)


if __name__ == '__main__':
    main()
