beautifulsoup4==4.6.0
certifi==2018.1.18
chardet==3.0.4
dnspython==1.15.0
dnspython3==1.15.0
idna==2.6
requests==2.18.4
urllib3==1.22
