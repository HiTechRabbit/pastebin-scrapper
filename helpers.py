import zipfile
import os
import socket
import smtplib
from dns import resolver


def is_email_address_valid(mail: str) -> bool:
    """
    S'assure que l'adresse e-mail existe réellement
    :param mail: Adresse e-mail à valider
    :return: si l'adresse est valide ou non
    """
    hostname = "@".split(mail)[-1]
    records = resolver.query(hostname)
    if not records:
        return False

    try:
        host = socket.gethostname()
        server = smtplib.SMTP()

        # Évite la méthode verify() car elle est bloquée sur la plupart des serveurs
        server.connect(records[0])
        server.helo(host)
        server.mail('badoo@yopmail.fr')
        code, message = server.rcpt(str(mail))
        server.quit()
    except smtplib.SMTPException:
        return False

    if code == 250:
        return True
    return False


def compress(source_path: str, destination_path: str):
    """
    Compresse les fichiers d'hier dans un archive ZIP.
    :param source_path: Dossier de téléchargement
    :param destination_path: Futur fichier ZIP
    """
    zip_file = zipfile.ZipFile(destination_path, "w", zipfile.ZIP_DEFLATED)
    abs_src = os.path.abspath(source_path)
    for directory_name, sub_directories, files in os.walk(source_path):
        for filename in files:
            if filename not in destination_path:
                absolute_name = os.path.abspath(os.path.join(directory_name, filename))
                arc_name = absolute_name[len(abs_src) + 1:]
                zip_file.write(absolute_name, arc_name)
    zip_file.close()